import "./App.css";
import styled from "styled-components";

const Wrapper = styled.div`
  display: flex;
  width: 100vw;
  align-items: center;
  column-gap: 1rem;
`;

const Box = styled.div`
  width: 16rem;
  height: 16rem;
`;

const Red = styled(Box)`
  background: red;
`;

const Blue = styled(Box)`
  background: blue;
`;

function App() {
  return (
    <Wrapper>
      <Red>I should be red.</Red>
      <Blue>I should be blue.</Blue>
    </Wrapper>
  );
}

export default App;
